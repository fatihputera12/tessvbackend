package data

import (
	"tsi-pagebuilder.com/api/app/model"
)

// AdminRepo admin repository
type AdminRepo interface {
	Add(admin *model.Admin) error
	Remove(adminID uint) error
	FindByID(adminID uint) (*model.Admin, error)
	FindByEmail(email string) (*model.Admin, error)
	ChangePassword(adminID uint, newPassword string) error
	IsEmailValid(email string, companyID uint) (bool, error)
	GetAdminsByAuthorizationActionKeys(companyID uint, adminAuthorizationActionKeys []string) ([]*model.Admin, error)

	AddAdmin(data *model.Admin) error
	SaveAdmin(data *model.Admin) error
	DeleteAdmin(email string) error
	GetByParams(page int, perPage int, search string) (*model.PaginationResponse, error)
}

type PostRepo interface {
	FindByID(postID uint) (*model.Post, error)
	CountStatus() ([]*model.Status, error)
	AddPost(data *model.Post) error
	SavePost(data *model.Post) error
	DeletePost(postID uint) error
	GetByParams(page int, perPage int, search string, status model.PostStatus) (*model.PaginationResponse, error)
}
