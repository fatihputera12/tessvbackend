package data

import (
	"errors"
	"math"

	"gorm.io/gorm"
	"tsi-pagebuilder.com/api/app/model"
)

type postRepoMysql struct {
	db *gorm.DB
}

// NewPostRepoMysql instantiate post repository backed by mysql
func NewPostRepoMysql(db *gorm.DB) PostRepo {
	return &postRepoMysql{
		db: db,
	}
}

func (r *postRepoMysql) FindByID(postID uint) (*model.Post, error) {
	var dest model.Post
	result := r.db.Model(&model.Post{}).Where("id = ?", postID).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *postRepoMysql) CountStatus() ([]*model.Status, error) {
	var dest []*model.Status
	var data []*model.Post
	// var count int64
	// var data model.Post
	// result := r.db.Select("post.category", "post.COUNT(*)").Find(dest).Group("post.category").Debug()
	result := r.db.Debug().Select("status", "Count(*) as total").Group("status").Model(&data).Find(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return dest, nil
}

func (r *postRepoMysql) AddPost(data *model.Post) error {
	if err := data.Validate(); err != nil {
		return err
	}
	if err := r.db.Create(data).Error; err != nil {
		return err
	}

	return nil
}

func (r *postRepoMysql) SavePost(data *model.Post) error {
	if err := data.Validate(); err != nil {
		return err
	}
	if err := r.db.Save(data).Error; err != nil {
		return err
	}

	return nil
}

func (r *postRepoMysql) DeletePost(postID uint) error {
	return r.db.Where("id = ?", postID).Delete(&model.Post{}).Error
}

func (r *postRepoMysql) GetByParams(page int, perPage int, search string, status model.PostStatus) (*model.PaginationResponse, error) {
	if page <= 0 {
		page = 1
	}
	if perPage <= 0 {
		perPage = 10
	}
	offset := (page - 1) * perPage

	query := r.db.Model(&model.Post{})
	if status != "" {
		query = query.Where("status = ?", status)
	}
	if search != "" {
		query.Where("title LIKE ? OR category LIKE ?", "%"+search+"%", "%"+search+"%")
	}

	// -------------------------------- count --------------------------------
	var count int64
	if err := query.Count(&count).Error; err != nil {
		return nil, err
	}
	// -------------------------------- data --------------------------------
	var data []*model.Post
	if err := query.Debug().Limit(perPage).Offset(offset).Find(&data).Error; err != nil {
		return nil, err
	}
	return &model.PaginationResponse{
		Data:      data,
		TotalItem: count,
		LastPage:  int64(math.Ceil(float64(count) / float64(perPage))),
		Page:      page,
		PerPage:   perPage,
	}, nil
}
