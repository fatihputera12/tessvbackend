package data

import (
	"errors"
	"fmt"
	"math"

	"gorm.io/gorm"
	"tsi-pagebuilder.com/api/app/model"
)

type adminRepoMysql struct {
	db *gorm.DB
}

// NewAdminRepoMysql instantiate admin repository backed by mysql
func NewAdminRepoMysql(db *gorm.DB) AdminRepo {
	return &adminRepoMysql{
		db: db,
	}
}

func (r *adminRepoMysql) Add(admin *model.Admin) error {
	return r.db.Create(admin).Error
}

func (r *adminRepoMysql) Remove(adminID uint) error {
	return r.db.Model(&model.Admin{}).Delete("id = ?", adminID).Error
}

func (r *adminRepoMysql) FindByID(adminID uint) (*model.Admin, error) {
	var dest model.Admin
	result := r.db.Model(&model.Admin{}).Where("id = ?", adminID).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *adminRepoMysql) FindByEmail(email string) (*model.Admin, error) {
	var dest model.Admin
	result := r.db.Model(&model.Admin{}).Where("email = ?", email).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *adminRepoMysql) ChangePassword(adminID uint, newPassword string) error {
	return r.db.Model(&model.Admin{}).Where("id = ?", adminID).Update("password", newPassword).Error
}

func (r *adminRepoMysql) IsEmailValid(email string, companyID uint) (bool, error) {
	var admin model.Admin
	if err := r.db.Where("company_id = ? AND email = ?", companyID, email).Find(&admin).Error; err != nil {
		return false, err
	}
	if admin.ID != 0 {
		return false, fmt.Errorf("Email is already used")
	}

	return true, nil
}

func (r *adminRepoMysql) GetAdminsByAuthorizationActionKeys(companyID uint, adminAuthorizationActionKeys []string) ([]*model.Admin, error) {
	var data []*model.Admin
	if err := r.db.Table("admins AS A").
		Joins("join admin_authorizations AS B ON A.id=B.admin_id").
		Where("A.company_id=?", companyID).
		Where("B.admin_authorization_action_key in (?) AND B.is_authorized = ?", adminAuthorizationActionKeys, true).
		Where("A.deleted_at IS NULL").
		Where("B.deleted_at IS NULL").
		Find(&data).Error; err != nil {
		return nil, err
	}
	return data, nil
}

func (r *adminRepoMysql) AddAdmin(data *model.Admin) error {
	return r.db.Create(data).Error
}

func (r *adminRepoMysql) SaveAdmin(data *model.Admin) error {
	return r.db.Save(data).Error
}

func (r *adminRepoMysql) DeleteAdmin(email string) error {
	return r.db.Where("email = ?", email).Delete(&model.Admin{}).Error
}

func (r *adminRepoMysql) GetByParams(page int, perPage int, search string) (*model.PaginationResponse, error) {
	if page <= 0 {
		page = 1
	}
	if perPage <= 0 {
		perPage = 10
	}
	offset := (page - 1) * perPage

	query := r.db.Model(&model.Admin{})
	if search != "" {
		query.Where("name LIKE ? OR email LIKE ?", "%"+search+"%", "%"+search+"%")
	}

	// -------------------------------- count --------------------------------
	var count int64
	if err := query.Count(&count).Error; err != nil {
		return nil, err
	}
	// -------------------------------- data --------------------------------
	var data []*model.Admin
	if err := query.Debug().Limit(perPage).Offset(offset).Find(&data).Error; err != nil {
		return nil, err
	}
	return &model.PaginationResponse{
		Data:      data,
		TotalItem: count,
		LastPage:  int64(math.Ceil(float64(count) / float64(perPage))),
		Page:      page,
		PerPage:   perPage,
	}, nil
}
