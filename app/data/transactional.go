package data

// Transactional factory for particular implementation of unit of work
type Transactional interface {
	// CreateUnitOfWork create unit of work and begin transactional operation
	CreateUnitOfWork() UnitOfWork
}

// UnitOfWork represent unit of work for each operation in each repository
type UnitOfWork interface {
	// GetAdminRepo return new admin repo that is coupled to this transactional unit of work
	GetAdminRepo() AdminRepo
	GetPostRepo() PostRepo
	// Complete commit all changes
	Complete() error
	// Rollback roll back operation
	Rollback() error
}
