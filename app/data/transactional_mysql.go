package data

import "gorm.io/gorm"

type transactionalMysql struct {
	db *gorm.DB
}

// NewTransactionalMysql create new transactional api for mysql repository
func NewTransactionalMysql(db *gorm.DB) Transactional {
	return &transactionalMysql{db: db}
}

func (t *transactionalMysql) CreateUnitOfWork() UnitOfWork {
	tx := t.db.Begin()
	return &unitOfWorkMysql{
		transaction: tx,
	}
}

type unitOfWorkMysql struct {
	transaction *gorm.DB
	adminRepo   AdminRepo
	postRepo    PostRepo
}

func (u *unitOfWorkMysql) GetAdminRepo() AdminRepo {
	if u.adminRepo == nil {
		// cache the value so we can call GetAdminRepo multiple times
		u.adminRepo = &adminRepoMysql{db: u.transaction}
	}
	return u.adminRepo
}

func (u *unitOfWorkMysql) GetPostRepo() PostRepo {
	if u.postRepo == nil {
		// cache the value so we can call GetPostRepo multiple times
		u.postRepo = &postRepoMysql{db: u.transaction}
	}
	return u.postRepo
}

func (u *unitOfWorkMysql) Complete() error {
	return u.transaction.Commit().Error
}

func (u *unitOfWorkMysql) Rollback() error {
	return u.transaction.Rollback().Error
}
