package model

import (
	"time"

	"gorm.io/gorm"
)

// Admin Model
type Admin struct {
	// gorm.Model
	ID        uint           `gorm:"primarykey" json:"id"`
	Email     string         `json:"email"`
	Password  string         `json:"-"`
	Name      string         `json:"name"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}
