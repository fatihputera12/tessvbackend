package model

//PaginationResponse model
type PaginationResponse struct {
	Data      interface{} `json:"data"`
	TotalItem int64       `json:"total_item"`
	LastPage  int64       `json:"last_page"`
	Page      int         `json:"page"`
	PerPage   int         `json:"per_page"`
}

type PaginationResponseForDrs struct {
	Data        interface{} `json:"data"`
	Total       int64       `json:"total"`
	LastPage    int64       `json:"last_page"`
	CurrentPage int         `json:"current_page"`
	PerPage     int         `json:"per_page"`
}
