package model

import (
	"fmt"
	"time"

	"gorm.io/gorm"
)

type PostStatus string

const (
	PostStatusDraft   PostStatus = "draft"
	PostStatusPublish PostStatus = "publish"
	PostStatusTrash   PostStatus = "trash"
)

// Post Model
type Post struct {
	// gorm.Model
	ID        uint           `gorm:"primarykey" json:"id"`
	Title     string         `json:"title"`
	Content   string         `json:"content"`
	Category  string         `json:"category"`
	Status    PostStatus     `json:"status"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

type Status struct {
	// gorm.Model
	Status string `json:"status"`
	Total  int64  `gorm:"->" json:"total"`
}

func (r *Post) Validate() error {
	switch r.Status {
	case PostStatusDraft:
	case PostStatusPublish:
	case PostStatusTrash:
	default:
		return fmt.Errorf("Wrong Status")
	}
	if len([]rune(r.Title)) < 20 {
		// return fmt.Errorf("Title min 20 character")
		return fmt.Errorf("Title min 20 character")
	}
	if len([]rune(r.Content)) < 200 {
		return fmt.Errorf("Content min 20 character")
	}
	if len([]rune(r.Category)) < 3 {
		return fmt.Errorf("Category min 3 character")
	}

	return nil

}
