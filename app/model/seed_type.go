package model

import (
	"gorm.io/gorm"
	"tsi-pagebuilder.com/api/config"
)

type SeedParameter struct {
	DB     *gorm.DB
	Config *config.Config
}

type SeedType struct {
	Name string
	Run  func(*SeedParameter) error
}
