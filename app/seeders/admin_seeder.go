package seeders

import (
	"tsi-pagebuilder.com/api/app/model"
	"tsi-pagebuilder.com/api/framework"
)

func adminSeeder(r *model.SeedParameter) error {

	admin := []model.Admin{
		{
			Email:    "admin@gmail.com",
			Password: "admin123",
			Name:     "admin",
		},
	}

	for i := range admin {
		password, err := framework.HashPassword(admin[i].Password)
		if err != nil {
			return err
		}
		admin[i].Password = password
	}

	if err := r.DB.Create(&admin).Error; err != nil {
		return err
	}

	return nil
}
