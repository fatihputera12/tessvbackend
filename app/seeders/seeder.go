package seeders

import "tsi-pagebuilder.com/api/app/model"

//SeederList array of functions
var SeederList = []model.SeedType{
	{Name: "Admin seeder", Run: adminSeeder},
}
