package provider

import (
	"github.com/go-redis/redis/v8"
	"tsi-pagebuilder.com/api/app/data"
	"tsi-pagebuilder.com/api/framework"
)

// Context store global resources access in application
type Context struct {
	RedisCli *redis.Client
	Cache    framework.Cache
	// Event                   framework.Event
	AccessTokenVerifier *framework.JWTVerifier
	Storage             framework.Storage
	AdminRepo           data.AdminRepo
	PostRepo            data.PostRepo
	Transactional       data.Transactional
}
