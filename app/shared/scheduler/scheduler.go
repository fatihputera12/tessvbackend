package scheduler

import "time"

type ScheduleHandlerContext struct {
	ActualExecTime   time.Time
	ExpectedExecTime time.Time
	Name             string
	CronSpec         string
	CreateErrorLog   func(err error) error
}

type ScheduleHandler func(context *ScheduleHandlerContext)

type Scheduler interface {
	ScheduleJob(name string, cronSpec string, handler ScheduleHandler) error
	Start()
	Stop()
}
