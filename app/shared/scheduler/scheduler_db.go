package scheduler

import (
	"log"
	"math/rand"
	"os"
	"time"

	"gorm.io/gorm/logger"

	"github.com/robfig/cron"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const maxDeSyncSeconds = 30
const schedulerEntrySQLMigration = `
CREATE TABLE IF NOT EXISTS scheduler_entries
(
	id bigint unsigned AUTO_INCREMENT,
	created_at datetime(3) NULL,
	name varchar(191),
	cron varchar(191),
	last_execution_at datetime(3) NULL,
	next_execution_at datetime(3) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX schedule_id (name),
	INDEX (next_execution_at)
);`

const schedulerErrorSQLMigration = `
CREATE TABLE IF NOT EXISTS scheduler_errors
(
	id bigint unsigned AUTO_INCREMENT,
	name varchar(191),
	description text,
	created_at datetime NULL,
	PRIMARY KEY (id)
);`

type (
	schedulerEntry struct {
		ID        uint
		CreatedAt time.Time

		Name            string
		Cron            string
		LastExecutionAt *time.Time
		NextExecutionAt time.Time

		//
		cronSchedule cron.Schedule `gorm:"->"`
	}

	schedulerError struct {
		ID          uint
		Name        string
		Description string
		CreatedAt   time.Time
	}

	schedulerDBImpl struct {
		db       *gorm.DB
		running  bool
		stopChan chan bool
		handlers map[string]ScheduleHandler
	}
)

func newSchedulerEntry(name, cronSpec string) (*schedulerEntry, error) {
	cronSchedule, err := cron.ParseStandard(cronSpec)
	if err != nil {
		return nil, err
	}

	entry := schedulerEntry{
		Name:         name,
		Cron:         cronSpec,
		cronSchedule: cronSchedule,
	}
	entry.UpdateNextExecutionFromNow()
	return &entry, nil
}

func (e *schedulerEntry) AfterFind(_ *gorm.DB) error {
	cronSchedule, err := cron.ParseStandard(e.Cron)
	e.cronSchedule = cronSchedule
	return err
}

func (e *schedulerEntry) UpdateNextExecutionFromNow() {
	if !e.NextExecutionAt.IsZero() {
		newTime := e.NextExecutionAt
		e.LastExecutionAt = &newTime
	}
	e.NextExecutionAt = e.cronSchedule.Next(time.Now())
}

func (e *schedulerEntry) GetDurationToNextExecution() time.Duration {
	return e.NextExecutionAt.Sub(time.Now())
}

func NewSchedulerDBImpl(db *gorm.DB, queryVerboseLog bool) (Scheduler, error) {
	logLevel := logger.Silent
	if queryVerboseLog {
		logLevel = logger.Info
	}
	gormLogger := logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
		LogLevel: logLevel,
	})
	newSession := db.Session(&gorm.Session{Logger: gormLogger})

	if err := setupDBTable(newSession); err != nil {
		return nil, err
	}

	return &schedulerDBImpl{
		db:       newSession,
		running:  false,
		stopChan: make(chan bool),
		handlers: make(map[string]ScheduleHandler),
	}, nil
}

func setupDBTable(db *gorm.DB) error {
	if err := db.Exec(schedulerEntrySQLMigration).Error; err != nil {
		return err
	}
	if err := db.Exec(schedulerErrorSQLMigration).Error; err != nil {
		return err
	}

	return nil
}

func (s *schedulerDBImpl) ScheduleJob(name string, cronSpec string, handler ScheduleHandler) error {
	entry, err := newSchedulerEntry(name, cronSpec)
	if err != nil {
		return err
	}

	// order matter
	var upsertUpdateColumns = []clause.Assignment{
		{
			Column: clause.Column{Name: "last_execution_at"},
			Value:  gorm.Expr("IF(cron <> VALUES(cron), null, last_execution_at)"),
		},
		{
			Column: clause.Column{Name: "next_execution_at"},
			Value:  gorm.Expr("IF(cron <> VALUES(cron), VALUES(next_execution_at), next_execution_at)"),
		},
		{
			Column: clause.Column{Name: "cron"},
			Value:  gorm.Expr("IF(cron <> VALUES(cron), VALUES(cron), cron)"),
		},
	}
	if err := s.db.
		Clauses(clause.OnConflict{DoUpdates: upsertUpdateColumns}).
		Create(entry).Error; err != nil {
		return err
	}

	s.handlers[name] = handler
	return nil
}

// Start loop and get overdue schedule to execute at a time
// the schedule maximum precision is at most 1 minute + de-synchronization random seconds
func (s *schedulerDBImpl) Start() {
	if s.running {
		return
	}
	s.running = true

	go func() {
		timer := time.NewTimer(time.Second)
		for {
			select {
			case <-timer.C:
				entry, err := s.findDueTimeSchedule()
				if err == gorm.ErrRecordNotFound {
					waitDuration := time.Minute + s.getRandomSecond()
					shortestEntry, err := s.findShortestWait()
					if err == nil {
						waitDuration = shortestEntry.GetDurationToNextExecution()
					}

					if waitDuration > time.Minute {
						waitDuration = time.Minute + s.getRandomSecond()
					}

					timer = time.NewTimer(waitDuration)
					break
				}

				if err != nil {
					timer = time.NewTimer((time.Minute * 2) + s.getRandomSecond())
					break
				}

				if handler, ok := s.handlers[entry.Name]; ok {
					execTime := time.Now()
					if entry.LastExecutionAt != nil {
						execTime = *entry.LastExecutionAt
					}
					go handler(&ScheduleHandlerContext{
						ActualExecTime:   time.Now(),
						ExpectedExecTime: execTime,
						Name:             entry.Name,
						CronSpec:         entry.Cron,
						CreateErrorLog: func(err error) error {
							logrus.Error(err)
							schedulerError := schedulerError{
								Name:        entry.Name,
								Description: err.Error(),
							}
							return s.db.Create(&schedulerError).Error
						},
					})

					timer = time.NewTimer(time.Second * 2)
				} else {
					timer = time.NewTimer(time.Second)
				}
			case <-s.stopChan:
				return
			}
		}
	}()
}

func (s *schedulerDBImpl) Stop() {
	if s.running {
		s.running = false
		s.stopChan <- true
	}
}

func (s *schedulerDBImpl) findDueTimeSchedule() (*schedulerEntry, error) {
	var entry schedulerEntry
	err := s.db.Transaction(func(tx *gorm.DB) error {
		if err := tx.
			Clauses(clause.Locking{Strength: "UPDATE"}).
			Where("next_execution_at < ?", time.Now()).
			First(&entry).Error; err != nil {
			return err
		}

		entry.UpdateNextExecutionFromNow()
		return tx.Save(&entry).Error
	})

	if err != nil {
		return nil, err
	}

	return &entry, nil
}

func (s *schedulerDBImpl) findShortestWait() (*schedulerEntry, error) {
	var entry schedulerEntry
	err := s.db.Clauses(clause.Locking{Strength: "SHARE", Options: "SKIP LOCKED"}).
		Where("next_execution_at < ?", time.Now().Add(time.Hour)).
		Order("next_execution_at ASC").
		First(&entry).Error
	if err != nil {
		return nil, err
	}

	return &entry, nil
}

func (s *schedulerDBImpl) getRandomSecond() time.Duration {
	return time.Duration(rand.Intn(maxDeSyncSeconds)) * time.Second
}
