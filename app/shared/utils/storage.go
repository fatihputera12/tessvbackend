package utils

import (
	"mime/multipart"
	"os"

	"github.com/h2non/filetype"

	// Package image/jpeg is not used explicitly in the code below,
	// but is imported for its initialization side-effect, which allows
	// image.Decode to understand JPEG formatted images. Uncomment these
	// two lines to also understand GIF and PNG images:

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	_ "golang.org/x/image/tiff"
	_ "golang.org/x/image/webp"
)

func getHeaderSection(fileHandle *multipart.FileHeader) ([]byte, error) {
	openedStream, err := fileHandle.Open()
	if err != nil {
		return nil, err
	}
	defer openedStream.Close()

	header := make([]byte, 261)
	openedStream.Read(header)
	return header, nil
}

// IsMultipartImageFile check if multiparth file header is an image file
func IsMultipartImageFile(fileHandle *multipart.FileHeader) (bool, error) {
	header, err := getHeaderSection(fileHandle)
	if err != nil {
		return false, err
	}
	return filetype.IsImage(header), nil
}

func IsMultipartVideoFile(fileHandle *multipart.FileHeader) (bool, error) {
	header, err := getHeaderSection(fileHandle)
	if err != nil {
		return false, err
	}
	return filetype.IsVideo(header), nil
}

// MkDirIfNotExists create directory including children directory if not exists
func MkDirIfNotExists(path string) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return os.MkdirAll(path, os.ModePerm)
	}

	return err
}

// IsFileExists check if given file path exists and not a directory
func IsFileExists(path string) bool {
	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !stat.IsDir()
}
