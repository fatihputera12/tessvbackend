package utils

import (
	"encoding/base64"
	"math"
	"math/rand"
	"mime/multipart"
	"reflect"
	"strconv"
	"time"
)

// StringToUint convert string to uint or zero if parsing failed
func StringToUint(str string) uint {
	parsedInt, err := strconv.ParseUint(str, 10, 32)
	if err != nil {
		return 0
	}

	return uint(parsedInt)
}

// StringToInt convert string to int or zero if parsing failed
func StringToInt(str string) int {
	parsedInt, err := strconv.ParseInt(str, 10, 32)
	if err != nil {
		return 0
	}

	return int(parsedInt)
}

// ExceedUploadLimit validate file size - parameter in byte
func ExceedUploadLimit(file *multipart.FileHeader, maxSize int64) bool {
	if file.Size > maxSize {
		return true
	}
	return false
}

func ConversionBytes(size float64) (fileSize string) {
	var suffixes [5]string

	suffixes[0] = "B"
	suffixes[1] = "KB"
	suffixes[2] = "MB"
	suffixes[3] = "GB"
	suffixes[4] = "TB"

	base := math.Log(size) / math.Log(1024)
	getSize := Round(math.Pow(1024, base-math.Floor(base)), .5, 0)
	getSuffix := suffixes[int(math.Floor(base))]
	fileSize = strconv.FormatFloat(getSize, 'f', -1, 64) + " " + string(getSuffix)

	return
}

func Round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

// func GeneratePassword() string {
// 	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ" +
// 		"abcdefghijklmnopqrstuvwxyzåäö" +
// 		"0123456789")
// 	length:=15
// 	var b strings.Builder
// 	for i := 0; i < length; i++ {
// 		b.WriteRune(chars[rand.Intn(len(chars))])
// 	}
// 	str := b.String()
// 	return str
// }

func GeneratePassword(length int) (string, error) {
	byteLength := 3 * length / 4
	b := make([]byte, byteLength)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return base64.RawStdEncoding.EncodeToString(b), nil
}

// FindInArray to validate if a value exists in an array
func FindInArray(arrayType interface{}, item interface{}) bool {
	arr := reflect.ValueOf(arrayType)

	if arr.Kind() != reflect.Array {
		panic("Invalid data-type")
	}

	for i := 0; i < arr.Len(); i++ {
		if arr.Index(i).Interface() == item {
			return true
		}
	}

	return false
}

func IntToPointerInt(number int) *int {
	return &number
}
func UintToPointerUint(number uint) *uint {
	return &number
}
func StringToPointerString(str string) *string {
	return &str
}
func BoolToPointerBool(value bool) *bool {
	return &value
}
func TimeToPointerTime(value time.Time) *time.Time {
	return &value
}
