package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"tsi-pagebuilder.com/api/app/model"
	"tsi-pagebuilder.com/api/app/shared/provider"
	"tsi-pagebuilder.com/api/framework"
)

type loginRequest struct {
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required"`
}
type loginResponse struct {
	User        *model.Admin `json:"user"`
	AccessToken string       `json:"access_token"`
}

type authHandler struct {
	ctx    *provider.Context
	jwtGen *JWTGenerator
}

func (h *authHandler) login(c *gin.Context) {
	var req loginRequest
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	user, err := h.ctx.AdminRepo.FindByEmail(req.Email)
	if err != nil || user == nil {
		c.JSON(http.StatusBadRequest, errorResp("error or no such user found", nil))
		return
	}

	if !framework.CheckPasswordHash(req.Password, user.Password) {
		c.JSON(http.StatusBadRequest, errorResp("credentials doesn't match", nil))
		return
	}

	accessToken, err := h.jwtGen.GenerateNew(user.ID)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, errorResp("can not generate user tokens", nil))
		return
	}

	// photoURL, _ := h.ctx.Storage.URL(user.Photo, true)
	resp := loginResponse{
		User:        user,
		AccessToken: accessToken,
	}
	c.JSON(http.StatusOK, successResp("login success", resp))
}

func (h *authHandler) createAdmin(c *gin.Context) {
	var req struct {
		Email    string `json:"email"`
		Password string `json:"password"`
		Name     string `json:"name"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	password, err := framework.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	admin := model.Admin{
		Email:    req.Email,
		Password: password,
		Name:     req.Name,
	}

	if err := h.ctx.AdminRepo.AddAdmin(&admin); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}
	c.JSON(http.StatusOK, successResp("create success", nil))
}

func (h *authHandler) updateAdmin(c *gin.Context) {
	var req struct {
		Email string `json:"email"`
		Name  string `json:"name"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	admin, err := h.ctx.AdminRepo.FindByEmail(req.Email)
	if admin == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Admin does not exist", nil))
		return
	}

	admin.Name = req.Name
	if err := h.ctx.AdminRepo.SaveAdmin(admin); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, successResp("update success", nil))
}

func (h *authHandler) deleteAdmin(c *gin.Context) {
	email := c.Param("email")

	authuser := GetAuthenticatedUser(c)

	if email == authuser.Email {
		c.JSON(http.StatusBadRequest, errorResp("Cannot delete self", nil))
		return
	}
	admin, err := h.ctx.AdminRepo.FindByEmail(email)
	if admin == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Admin does not exist", nil))
		return
	}

	if err := h.ctx.AdminRepo.DeleteAdmin(email); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp("Admin does not exist", nil))
		return
	}
	c.JSON(http.StatusOK, successResp("delete success", nil))
}

func (h *authHandler) viewAdmins(c *gin.Context) {
	var req struct {
		Page    int    `form:"page"`
		PerPage int    `form:"per_page"`
		Search  string `form:"search"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	data, err := h.ctx.AdminRepo.GetByParams(req.Page, req.PerPage, req.Search)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, successResp("get success", data))
}

func (h *authHandler) updatePassword(c *gin.Context) {
	var req struct {
		OldPassword string `json:"old_password"`
		NewPassword string `json:"new_password"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}
	authuser := GetAuthenticatedUser(c)

	// ----------------------- Check old password -----------------------
	matchpass := framework.CheckPasswordHash(req.OldPassword, authuser.Password)
	if !matchpass {
		c.JSON(http.StatusBadRequest, errorResp("Old password is wrong", nil))
		return
	}
	if req.OldPassword == req.NewPassword {
		c.JSON(http.StatusBadRequest, errorResp("Password does not change", nil))
		return
	}
	// ------------------------ New password ------------------------
	password, err := framework.HashPassword(req.NewPassword)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	authuser.Password = password
	if err := h.ctx.AdminRepo.SaveAdmin(authuser); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}
	c.JSON(http.StatusOK, successResp("update password success", nil))
}
