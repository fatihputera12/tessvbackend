package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"tsi-pagebuilder.com/api/app/model"
	"tsi-pagebuilder.com/api/app/shared/provider"
	"tsi-pagebuilder.com/api/app/shared/utils"
	"tsi-pagebuilder.com/api/config"
)

type postHandler struct {
	ctx    *provider.Context
	config *config.Config
}

type status struct {
	Draft   int64 `json:"draft"`
	Publish int64 `json:"publish"`
	Trash   int64 `json:"trash"`
}

func (h *postHandler) createPost(c *gin.Context) {
	var req struct {
		Title    string           `json:"title"`
		Content  string           `json:"content"`
		Category string           `json:"category"`
		Status   model.PostStatus `json:"status"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	post := model.Post{
		Title:    req.Title,
		Content:  req.Content,
		Category: req.Category,
		Status:   req.Status,
	}

	if err := h.ctx.PostRepo.AddPost(&post); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}
	c.JSON(http.StatusOK, successResp("create success", nil))
}

func (h *postHandler) updatePost(c *gin.Context) {
	var req struct {
		Title    string           `json:"title"`
		Content  string           `json:"content"`
		Category string           `json:"category"`
		Status   model.PostStatus `json:"status"`
	}
	ID := utils.StringToUint(c.Param("ID"))
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	post, err := h.ctx.PostRepo.FindByID(ID)
	if post == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Post does not exist", nil))
		return
	}

	if req.Title != "" && req.Content != "" && req.Category != "" {
		post.Title = req.Title
		post.Content = req.Content
		post.Category = req.Category
	}

	post.Status = req.Status

	if err := h.ctx.PostRepo.SavePost(post); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, successResp("update success", nil))
}

func (h *postHandler) updatePostStatus(c *gin.Context) {
	var req struct {
		Status model.PostStatus `json:"status"`
	}
	ID := utils.StringToUint(c.Param("ID"))
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	post, err := h.ctx.PostRepo.FindByID(ID)
	if post == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Post does not exist", nil))
		return
	}

	post.Status = req.Status
	if err := h.ctx.PostRepo.SavePost(post); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, successResp("update success", nil))
}

func (h *postHandler) deletePost(c *gin.Context) {
	ID := utils.StringToUint(c.Param("ID"))
	post, err := h.ctx.PostRepo.FindByID(ID)
	if post == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Post does not exist", nil))
		return
	}

	if err := h.ctx.PostRepo.DeletePost(ID); err != nil {
		c.JSON(http.StatusInternalServerError, errorResp("Post does not exist", nil))
		return
	}
	c.JSON(http.StatusOK, successResp("delete success", nil))
}

func (h *postHandler) viewPosts(c *gin.Context) {
	var req struct {
		Page    int              `json:"page" form:"page"`
		PerPage int              `json:"per_page" form:"per_page"`
		Search  string           `json:"search" form:"search"`
		Status  model.PostStatus `json:"status" form:"status"`
	}
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error(), nil))
		return
	}

	if req.Status != model.PostStatusDraft && req.Status != model.PostStatusPublish && req.Status != model.PostStatusTrash && req.Status != "" {
		c.JSON(http.StatusBadRequest, errorResp("Status invalid", nil))
		return
	}

	data, err := h.ctx.PostRepo.GetByParams(req.Page, req.PerPage, req.Search, req.Status)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, successResp("get success", data))
}

func (h *postHandler) getPostDetail(c *gin.Context) {
	ID := utils.StringToUint(c.Param("ID"))
	post, err := h.ctx.PostRepo.FindByID(ID)
	if post == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Post does not exist", nil))
		return
	}

	c.JSON(http.StatusOK, successResp("succes", post))
}

func (h *postHandler) getPostMasterData(c *gin.Context) {

	post, err := h.ctx.PostRepo.CountStatus()
	if post == nil || err != nil {
		c.JSON(http.StatusBadRequest, errorResp("Post does not exist", nil))
		return
	}

	var status status
	for _, v := range post {
		if v.Status == string(model.PostStatusDraft) {
			status.Draft = v.Total
		}
		if v.Status == string(model.PostStatusPublish) {
			status.Publish = v.Total
		}
		if v.Status == string(model.PostStatusTrash) {
			status.Trash = v.Total
		}

	}

	type DashboardResponse struct {
		Status interface{}
	}
	Response := DashboardResponse{
		Status: status,
	}

	c.JSON(http.StatusOK, successResp("succes", Response))
}
