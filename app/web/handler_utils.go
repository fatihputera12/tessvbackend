package web

type baseResponse struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func errorResp(errorMessage string, data interface{}) baseResponse {
	return baseResponse{
		Success: false,
		Message: errorMessage,
		Data:    data,
	}
}

func successResp(message string, data interface{}) baseResponse {
	return baseResponse{
		Success: true,
		Message: message,
		Data:    data,
	}
}
