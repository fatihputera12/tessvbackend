package web

import (
	"fmt"
	"time"

	"tsi-pagebuilder.com/api/framework"

	"github.com/lestrrat-go/jwx/jwt"
)

// JWTGenerator a service use case for authenticating user
type JWTGenerator struct {
	AccessTokenVerif  *framework.JWTVerifier
	RefreshTokenVerif *framework.JWTVerifier
}

func (r *JWTGenerator) generateAccessToken(userID uint) (string, error) {
	jwtClaims := map[string]interface{}{
		jwt.IssuedAtKey:   time.Now().Unix(),
		jwt.ExpirationKey: time.Now().Add(time.Hour * 168).Unix(), //7 days
		"uid":             fmt.Sprintf("%d", userID),
	}
	return r.AccessTokenVerif.GenerateJWT(jwtClaims)
}

func (r *JWTGenerator) generateRefreshToken(userID uint) (string, error) {
	refreshTokenClaims := map[string]interface{}{
		jwt.IssuedAtKey:   time.Now().Unix(),
		jwt.ExpirationKey: time.Now().Add(time.Minute * 10).Unix(),
		"uid":             fmt.Sprintf("%d", userID),
	}
	return r.RefreshTokenVerif.GenerateJWT(refreshTokenClaims)
}

// GenerateNew returns (accessToken, error)
func (r *JWTGenerator) GenerateNew(userID uint) (string, error) {
	accessToken, err := r.generateAccessToken(userID)
	if err != nil {
		return "", err
	}

	return accessToken, err
}

// RefreshAccessToken returns (accessToken, error)
func (r *JWTGenerator) RefreshAccessToken(userID uint) (string, error) {
	accessToken, err := r.generateAccessToken(userID)
	if err != nil {
		return "", err
	}

	return accessToken, nil
}
