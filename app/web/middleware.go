package web

import (
	"fmt"
	"net/http"
	"strings"

	"tsi-pagebuilder.com/api/framework"

	"github.com/gin-gonic/gin"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/sirupsen/logrus"
	"tsi-pagebuilder.com/api/app/data"
	"tsi-pagebuilder.com/api/app/model"
	"tsi-pagebuilder.com/api/app/shared/utils"
)

// AuthenticateAdminObjectKey key for authenticated admin
const AuthenticateAdminObjectKey = "auth-admin"

// VerifiedTokenObjectKey key for accessing verified jwt result
const VerifiedTokenObjectKey = "verified-token"

// AuthenticatedCompanyDrsObjectKey key for company drs
const AuthenticatedCompanyDrsObjectKey = "auth-company-drs"
const AuthenticatedCompanyIntraServerObjectKey = "auth-company-intra-server"

// GetAuthenticatedUser return authenticated admin from request
func GetAuthenticatedUser(ctx *gin.Context) *model.Admin {
	authenticatedAdmin := ctx.Value(AuthenticateAdminObjectKey)
	if authenticatedAdmin == nil {
		return nil
	}
	return authenticatedAdmin.(*model.Admin)
}

// GetVerifiedToken return verified jwt result from request
func GetVerifiedToken(ctx *gin.Context) jwt.Token {
	token := ctx.Value(VerifiedTokenObjectKey)
	if token == nil {
		return nil
	}
	return token.(jwt.Token)
}

// JWTAuthentication return jwt auth gin middleware
func JWTAuthentication(adminRepo data.AdminRepo, verif *framework.JWTVerifier) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := strings.TrimSpace(ctx.GetHeader("Authorization"))
		bearerTagLen := len("Bearer")

		if authHeader == "" || len(authHeader) < bearerTagLen {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, errorResp("authorization header not provided", nil))
			return
		}

		tokenString := authHeader[bearerTagLen:]
		token, err := verif.VerifyJWT(tokenString)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResp("token verification failed", nil))
			return
		}

		adminID, ok := token.Get("uid")
		if !ok {
			ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, errorResp("invalid provided token", nil))
			return
		}

		admin, err := adminRepo.FindByID(utils.StringToUint(fmt.Sprintf("%s", adminID)))
		if err != nil || admin == nil {
			logrus.Debugf("admin not found %s", adminID)
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResp("admin not found", nil))
			return
		}

		ctx.Set(VerifiedTokenObjectKey, token)
		ctx.Set(AuthenticateAdminObjectKey, admin)
		ctx.Next()
	}
}
