package web

import (
	"tsi-pagebuilder.com/api/app/shared/provider"

	"github.com/gin-gonic/gin"
	"tsi-pagebuilder.com/api/config"
)

// Router initialize http routes handlers
func Router(ctx *provider.Context, cfg *config.Config, engine *gin.Engine) {

	authHandler := authHandler{
		ctx: ctx,
		jwtGen: &JWTGenerator{
			AccessTokenVerif: ctx.AccessTokenVerifier,
		},
	}
	postHandler := postHandler{
		ctx:    ctx,
		config: cfg,
	}

	// -------------------------------- api setup --------------------------------
	publicAPI := engine.Group("/api")

	guardedAPI := engine.Group("/api")
	guardedAPI.Use(JWTAuthentication(ctx.AdminRepo, ctx.AccessTokenVerifier))

	// -------------------------------- auth --------------------------------
	publicAPI.POST("/auth/login", authHandler.login)
	guardedAPI.POST("/auth/password", authHandler.updatePassword)

	guardedAPI.POST("/admin/create", authHandler.createAdmin)
	guardedAPI.POST("/admin/update", authHandler.updateAdmin)
	guardedAPI.DELETE("/admin/delete/:email", authHandler.deleteAdmin)
	guardedAPI.GET("/admin/view", authHandler.viewAdmins)

	publicAPI.POST("/article", postHandler.createPost)
	publicAPI.POST("/article/:ID", postHandler.updatePost)
	publicAPI.DELETE("/article/:ID", postHandler.deletePost)
	publicAPI.GET("/article/", postHandler.viewPosts)
	publicAPI.GET("/article/:ID", postHandler.getPostDetail)
	publicAPI.GET("/article/MasterData", postHandler.getPostMasterData)

}
