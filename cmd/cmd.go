package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "api",
	Short: "Run backend api server",
	Run: func(cmd *cobra.Command, args []string) {
		// serve()
		serveApi()
	},
}

var seedDatabase = &cobra.Command{
	Use:   "seed",
	Short: "seed database",
	Run: func(cmd *cobra.Command, args []string) {
		runSeederCommand()
	},
}

var genPrivateKeyCmd = &cobra.Command{
	Use:   "genkey",
	Short: "Generate RSA private key (2048) which can be used as JWT sign/encrypt key",
	Run: func(cmd *cobra.Command, args []string) {
		generateKey()
	},
}

var migrateDatabase = &cobra.Command{
	Use:   "migrate",
	Short: "migrate database",
	Run: func(cmd *cobra.Command, args []string) {
		if fresh, _ := cmd.Flags().GetBool("fresh"); fresh {
			migrationDownCommand()
		}
		migrationUpCommand()

		if seed, _ := cmd.Flags().GetBool("seed"); seed { // if status is true, call seeder
			runSeederCommand()
		}
	},
}

// Execute execute root command
func Execute() {
	// here add additional command if exists
	rootCmd.AddCommand(genPrivateKeyCmd, migrateDatabase, seedDatabase)
	migrateDatabase.Flags().BoolP("fresh", "f", false, "Run migrate down")
	migrateDatabase.Flags().BoolP("seed", "s", false, "Run seeder")
	err := rootCmd.Execute()
	if err != nil {
		logrus.Panicf("Error execute(): %s\n", err)
		panic(err)
	}
}
