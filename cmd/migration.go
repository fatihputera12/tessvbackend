package cmd

import (
	"fmt"
	"os/exec"

	"github.com/sirupsen/logrus"
)

func migrationCommand(verbose []string) {
	cfg := getConfig()

	setupLoggerMisc(cfg)

	connCredential := fmt.Sprintf("mysql://%s:%s@(%s:%v)/%s", cfg.Database.UserName, cfg.Database.Password, cfg.Database.Host, cfg.Database.Port, cfg.Database.Name)

	args := []string{"-path", cfg.Command.MigratePath, "-database", connCredential, "-verbose"}

	for _, v := range verbose {
		args = append(args, v)
	}

	cmd := exec.Command(cfg.Command.MigrateApp, args...)
	logrus.Println(cmd.String()) // print the inputted command

	out, err := cmd.CombinedOutput()

	if err != nil {
		logrus.Errorln(string(out))
		logrus.Fatal(err)
	}
	logrus.Println(string(out)) //print the output
}

func migrationUpCommand() {
	verbose := []string{"up"}

	migrationCommand(verbose)
}

func migrationDownCommand() {
	verbose := []string{"down", "-all"}

	migrationCommand(verbose)
}
