package cmd

import (
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"tsi-pagebuilder.com/api/app/model"
	"tsi-pagebuilder.com/api/app/seeders"
)

type schemaSeeder struct {
	ID        uint
	Name      string
	CreatedAt time.Time
}

func getAllSeeded(db *gorm.DB) map[string]struct{} {
	var x []schemaSeeder
	if err := db.Find(&x).Error; err != nil {
		panic(err)
	}

	var y = make(map[string]struct{})
	for _, v := range x {
		y[v.Name] = struct{}{}
	}
	return y
}

func saveToList(db *gorm.DB, name string) {
	var x = schemaSeeder{
		Name: name,
	}
	if err := db.Create(&x).Error; err != nil {
		panic(err)
	}
}

func runSeederCommand() {
	cfg := getConfig()
	db := createDatabaseConnection(cfg)

	setupLoggerMisc(cfg)

	var list []model.SeedType
	list = append(list, seeders.SeederList...)

	seeded := getAllSeeded(db)
	seedParameter := model.SeedParameter{
		DB:     db,
		Config: cfg,
	}

loop:
	for _, v := range list {
		if _, ok := seeded[v.Name]; !ok {
			funcName := v.Name
			if err := v.Run(&seedParameter); err != nil {
				logrus.Errorf("Running seed '%s', failed with error: %s\n", funcName, err)
				break loop
			} else {
				logrus.Infof("%s run successfully\n", funcName)
				saveToList(db, v.Name)
			}
		}

	}
}
