package cmd

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"tsi-pagebuilder.com/api/app/data"
	"tsi-pagebuilder.com/api/app/shared/provider"
	"tsi-pagebuilder.com/api/app/web"
	"tsi-pagebuilder.com/api/fileserver"
	"tsi-pagebuilder.com/api/framework"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"tsi-pagebuilder.com/api/config"
)

func createStorage(cfg *config.Config, engine *gin.Engine) framework.Storage {
	storageType := cfg.Storage
	switch {
	case storageType == "oss":
		return framework.NewStorageAlibabaOSS(
			cfg.AlibabaOSS.BucketName,
			cfg.AlibabaOSS.Endpoint,
			cfg.AlibabaOSS.AccessID,
			cfg.AlibabaOSS.AccessSecret)
	case storageType == "s3":
		return framework.NewStorageAWSS3(
			cfg.S3.BucketName,
			cfg.AWS.Region,
			cfg.AWS.AccessKeyID,
			cfg.AWS.SecretAccessKey,
			cfg.AWS.SessionToken)
	case storageType == "local":
		fileServer, storage := fileserver.NewLocalFileServer(cfg.LocalStorage.BaseURL, cfg)

		if fileServer != nil && engine != nil {
			fileServer.InitHTTPRouter(engine)
		}

		return storage
	}
	return nil
}

func serveApi() {
	cfg := getConfig()
	setupLogger(cfg.Mode)

	db := createDatabaseConnection(cfg)
	redisCli := createRedisClient(cfg)

	// newScheduler, err := scheduler.NewSchedulerDBImpl(db, false)
	// if err != nil {
	// 	panic(err)
	// }

	engine := gin.Default()

	storage := createStorage(cfg, engine)

	adminRepo := data.NewAdminRepoMysql(db)
	postRepo := data.NewPostRepoMysql(db)
	accessTokenVerifier := framework.NewJWTVerifier(cfg.JWT.AccessTokenKeyPath)
	cache := framework.NewCacheRedis(redisCli, cfg.AppName)
	transactional := data.NewTransactionalMysql(db)

	appCtx := &provider.Context{
		RedisCli: redisCli,
		Cache:    cache,
		// Event:                event,
		AccessTokenVerifier: accessTokenVerifier,
		Storage:             storage,
		AdminRepo:           adminRepo,
		PostRepo:            postRepo,
		Transactional:       transactional,
	}

	engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Range", "Authorization"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	}))

	// engine.StaticFS("public/files", http.Dir(cfg.LocalStorage.PublicBaseDir))

	web.Router(appCtx, cfg, engine)

	hostAddress := fmt.Sprintf("%s:%s", cfg.Server.Host, cfg.Server.Port)
	srv := &http.Server{
		Addr:    hostAddress,
		Handler: engine,
	}

	go func() {
		logrus.Infof("%s running in %s mode at %s\n", cfg.AppName, cfg.Mode, hostAddress)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	logrus.Infoln("Gracefully shutting down server... [max delay 5 secs]")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logrus.Fatal("Server forced to shutdown: ", err)
	}

	logrus.Println("Server exiting")
}
