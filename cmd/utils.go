package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"tsi-pagebuilder.com/api/app/shared/utils"
	"tsi-pagebuilder.com/api/config"
)

func getConfig() *config.Config {
	viper.SetConfigName("config")
	viper.AddConfigPath("$HOME/")
	viper.AddConfigPath("/")
	viper.AddConfigPath(".")

	viper.BindEnv("mode", "APP_MODE")
	viper.BindEnv("appname", "APP_NAME")

	// Server listener address
	viper.BindEnv("server.host", "SERVER_HOST")
	viper.BindEnv("server.port", "SERVER_PORT")

	// Database address and credentials
	viper.BindEnv("database.host", "DB_HOST")
	viper.BindEnv("database.port", "DB_PORT")
	viper.BindEnv("database.username", "DB_USER")
	viper.BindEnv("database.password", "DB_PASSWORD")
	viper.BindEnv("database.name", "DB_NAME")

	// Redis config
	viper.BindEnv("redis.address", "REDIS_ADDRESS")
	viper.BindEnv("redis.password", "REDIS_PASSWORD")

	// // Email config
	// viper.BindEnv("email.host", "EMAIL_HOST")
	// viper.BindEnv("email.port", "EMAIL_PORT")
	// viper.BindEnv("email.username", "EMAIL_USERNAME")
	// viper.BindEnv("email.password", "EMAIL_PASSWORD")

	// Alibaba OSS config
	viper.BindEnv("alibabaoss.endpoint", "OSS_ENDPOINT")
	viper.BindEnv("alibabaoss.accessid", "OSS_ACCESS_ID")
	viper.BindEnv("alibabaoss.accessSecret", "OSS_ACCESS_SECRET")
	viper.BindEnv("alibabaoss.bucketName", "OSS_BUCKET_NAME")

	// AWS S3 config
	viper.BindEnv("s3.bucketName", "AWS_S3_BUCKET_NAME")

	// AWS credentials
	viper.BindEnv("aws.region", "AWS_REGION")
	viper.BindEnv("aws.accessKeyID", "AWS_ACCESS_KEY_ID")
	viper.BindEnv("aws.secretAccessKey", "AWS_SECRET_ACCESS_KEY")
	viper.BindEnv("aws.sessionToken", "AWS_SESSION_TOKEN")

	// Local storage config
	viper.BindEnv("localstorage.basedir", "LOCAL_STORAGE_BASEDIR")
	viper.BindEnv("localstorage.publicBaseDir", "LOCAL_STORAGE_PUBLIC_BASEDIR")
	viper.BindEnv("localstorage.baseUrl", "LOCAL_STORAGE_BASE_URL")

	// JWT key config
	viper.BindEnv("jwt.accessTokenKeyPath", "JWT_ACCESS_TOKEN_KEY_PATH")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	var cfg config.Config
	err = viper.Unmarshal(&cfg)
	if err != nil {
		panic(err)
	}

	return &cfg
}

func createDatabaseConnection(config *config.Config) *gorm.DB {
	databaseHost := fmt.Sprintf("%s:%d", config.Database.Host, config.Database.Port)
	dbURL := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		config.Database.UserName,
		config.Database.Password,
		databaseHost,
		config.Database.Name)
	db, err := gorm.Open(mysql.Open(dbURL), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	return db
}

func createRedisClient(cfg *config.Config) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Address,
		Password: cfg.Redis.Password,
		DB:       0,
	})
}

func setupLogger(mode config.EnvMode) {

	level := logrus.DebugLevel
	if mode == config.ModeProd {
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)

	logFolder := "general"
	setupLogrusTextFormatter(logFolder)

}

func setupLoggerMisc(cfg *config.Config) {
	if cfg.Mode == config.ModeProd {
		logrus.SetLevel(logrus.InfoLevel)
		logrus.SetFormatter(&logrus.JSONFormatter{})
	} else {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true, DisableColors: false})
	}
}

func setupLogrusTextFormatter(logFolder string) {
	folder := fmt.Sprintf("storage/logs/%s", logFolder)
	if err := utils.MkDirIfNotExists(folder); err != nil {
		panic(err)
	}
	path := fmt.Sprintf("%s/%s.log", folder, time.Now().Format("2006_01_02_150405"))
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	logrus.SetOutput(f)
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true, FullTimestamp: true})
}
