package config

// EnvMode environment mode
type EnvMode string

const (
	// ModeDevelop running in develop mode
	ModeDevelop EnvMode = "develop"
	// ModeProd running in production mode
	ModeProd EnvMode = "prod"
)

// Config store configuration value
type Config struct {
	// Mode determines the environment on which this
	// applications run, for example this may changed
	// the behaviour of the logger output and formatting
	Mode EnvMode

	// AppName can be used to identify your applications
	// for example used as a prefix in a cache store or queue
	AppName string

	// Storage: oss, s3, local
	Storage string

	Server struct {
		// Host is used by this application to listen to the incoming tcp connection
		// e.g. localhost, or 0.0.0.0 to listen on all network interface,
		// therefore it may be exposed outside
		Host string
		// Port it's port number where this application is listening on
		// e.g. port 8080, 80 etc.
		Port string
	}

	Database struct {
		// Host is server host name (domain name) or ip where database is running
		Host string
		// Port is the database host port number
		Port uint16
		// UserName is database credential user name used to access db
		UserName string
		// Password is database credential password used to access db
		Password string
		// Name is the name of database
		Name string
	}

	Redis struct {
		// Address is redis server host address including its port number
		// e.g. localhost:6379
		Address string
		// Password is redis account password used to access redis server
		Password string
	}

	// AlibabaOSS is configuration storage backed by Alibaba OSS
	// for more info https://www.alibabacloud.com/help/product/31815.htm
	AlibabaOSS struct {
		// Endpoint is OSS
		// for more detail https://www.alibabacloud.com/help/doc-detail/31837.htm
		Endpoint string
		// AccessID is alibaba RAM access key id
		AccessID string
		// AccessSecret is alibaba RAM access key secret
		AccessSecret string
		// BucketName is OSS bucket name
		BucketName string
	}

	// S3 is aws s3 configuration
	// 	// for more info https://docs.aws.amazon.com/sdk-for-go/api/service/s3/
	S3 struct {
		// BucketName is S3 bucket name
		BucketName string
	}

	// AWS is configuration for aws sdk
	// for more info https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/configuring-sdk.html
	AWS struct {
		// Region is aws selected region
		Region string
		// AccessKeyID is aws IAM access key id
		AccessKeyID string
		// SecretAccessKey is aws IAM secret access key id
		SecretAccessKey string
		// SessionToken is for temporary security credentials token or just left blank
		SessionToken string
	}

	LocalStorage struct {
		// BaseDir is an absolute or relative path to base directory inside local disk
		BaseDir string
		// PublicBaseDir is an absolute or relative path to base directory
		// inside local disk that is used as public file storage
		// which is served by the http server and are accessible publicly
		PublicBaseDir string
		// BaseURL is this application url endpoint in which serve the public file directory
		// this value used to create url for object in local file storage
		BaseURL string

		SigneURLSecret string
	}

	JWT struct {
		// AccessTokenKeyPath is the absolute or relative path to
		// a private key (.pem) used for generating and verifying access token
		AccessTokenKeyPath string
	}

	Command struct {
		MigrateApp  string
		MigratePath string
	}
}
