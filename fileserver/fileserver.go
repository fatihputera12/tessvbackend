package fileserver

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"tsi-pagebuilder.com/api/config"
	"tsi-pagebuilder.com/api/framework"
)

const (
	paramSignature = "signature"
	paramExpireAt  = "expireAt"
)

type FileServer struct {
	secret    string
	appDomain string

	publicFileURLPath  string
	privateFileURLPath string

	publicBaseFilePath  string
	privateBaseFilePath string
}

// NewLocalFileServer will return FileServer object and local file gostorage.Storage
func NewLocalFileServer(appDomain string, cfg *config.Config) (*FileServer, framework.Storage) {
	fs := &FileServer{
		secret:              cfg.LocalStorage.SigneURLSecret,
		appDomain:           appDomain,
		publicFileURLPath:   cfg.LocalStorage.PublicBaseDir,
		privateFileURLPath:  "/var/www/storage/private",
		publicBaseFilePath:  cfg.LocalStorage.PublicBaseDir,
		privateBaseFilePath: cfg.LocalStorage.BaseDir,
	}

	storage := framework.NewStorageLocalFile(
		fs.privateBaseFilePath,
		fs.publicBaseFilePath,
		appDomain,
		fs.signedURLBuilder)

	return fs, storage
}

func (f *FileServer) InitHTTPRouter(engine *gin.Engine) {
	r := engine.Group("/")
	r.StaticFS(f.publicFileURLPath, http.Dir(f.publicBaseFilePath))
	// r.Use(f.signedURLMiddleware).StaticFS(f.privateFileURLPath, http.Dir(f.privateBaseFilePath))
}

func (f *FileServer) signedURLBuilder(absoluteFilePath string, objectPath string, expireIn time.Duration) (string, error) {
	u, err := url.Parse(f.appDomain + f.privateFileURLPath)
	if err != nil {
		return "", err
	}

	expireAt := fmt.Sprintf("%d", time.Now().Add(expireIn).Unix())

	q := u.Query()
	q.Add(paramExpireAt, expireAt)

	u.Path = path.Join(u.Path, objectPath)
	u.RawQuery = q.Encode()

	signature, err := f.generateHmac(expireAt, u.RequestURI())
	if err != nil {
		return "", err
	}

	q.Add(paramSignature, signature)
	u.RawQuery = q.Encode()

	return u.String(), nil
}

func (f *FileServer) signedURLMiddleware(context *gin.Context) {
	expireAt := context.Query(paramExpireAt)
	expire, err := strconv.ParseInt(expireAt, 10, 64)
	if err != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, "Invalid expiration parameter")
		return
	}

	if time.Now().Unix() > expire {
		context.AbortWithStatusJSON(http.StatusBadRequest, "URL already expired")
		return
	}

	u := context.Request.URL
	q := u.Query()

	signature := q.Get(paramSignature)
	q.Del(paramSignature)
	u.RawQuery = q.Encode()

	generatedSignature, err := f.generateHmac(expireAt, u.RequestURI())

	if generatedSignature != signature {
		context.AbortWithStatusJSON(http.StatusForbidden, "Not allowed to access this resource")
		return
	}
}

func (f *FileServer) generateHmac(expireAt string, requestURI string) (string, error) {
	data, err := json.Marshal(map[string]interface{}{
		"expireAt":   expireAt,
		"requestURI": requestURI,
	})
	if err != nil {
		return "", err
	}

	h := hmac.New(sha512.New, []byte(f.secret))
	h.Write(data)
	return base64.RawURLEncoding.EncodeToString(h.Sum(nil)), nil
}
