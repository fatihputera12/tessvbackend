package framework

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwe"
	"github.com/lestrrat-go/jwx/jwt"
)

// JWTVerifier generate and verify JWT
type JWTVerifier struct {
	privateKey *rsa.PrivateKey
}

// NewJWTVerifier create new Jwt verification
func NewJWTVerifier(privateKeyPath string) *JWTVerifier {
	file, err := os.Open(privateKeyPath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	privateKey := getRSAPrivateKey(data)
	return &JWTVerifier{
		privateKey: privateKey,
	}
}

// JWTClaims store all jwt claims
type JWTClaims map[string]interface{}

func getRSAPrivateKey(pemPrivateKey []byte) *rsa.PrivateKey {
	block, _ := pem.Decode(pemPrivateKey)
	key, _ := x509.ParsePKCS1PrivateKey(block.Bytes)
	return key
}

// GenerateJWT create new jwe message (sign and encrypt)
func (j *JWTVerifier) GenerateJWT(claims JWTClaims) (string, error) {
	t := jwt.New()
	for k, v := range claims {
		t.Set(k, v)
	}

	signed, err := jwt.Sign(t, jwa.RS256, j.privateKey)
	if err != nil {
		return "", err
	}

	encrypted, err := jwe.Encrypt([]byte(signed), jwa.RSA1_5, &j.privateKey.PublicKey, jwa.A128CBC_HS256, jwa.NoCompress)
	if err != nil {
		return "", err
	}

	return string(encrypted), nil
}

// VerifyJWT decrypt and verify string jwe message in token
func (j *JWTVerifier) VerifyJWT(token string) (jwt.Token, error) {
	decrypted, err := jwe.Decrypt([]byte(token), jwa.RSA1_5, j.privateKey)
	if err != nil {
		return nil, err
	}

	t, err := jwt.Parse(decrypted, jwt.WithVerify(jwa.RS256, j.privateKey.PublicKey))
	if err != nil {
		return nil, err
	}

	err = jwt.Validate(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}
