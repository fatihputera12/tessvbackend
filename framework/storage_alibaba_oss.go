package framework

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"time"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

const signedURLExpiration = 30 * 60 // 30 minutes

type storageAlibabaOSS struct {
	client *oss.Client
	bucket *oss.Bucket
}

// NewStorageAlibabaOSS create storage backed by alibaba oss
func NewStorageAlibabaOSS(
	bucketName string,
	endpoint string,
	accessID string,
	accessSecret string) Storage {

	client, err := oss.New(endpoint, accessID, accessSecret)
	if err != nil {
		panic(err)
	}

	bucket, err := client.Bucket(bucketName)
	if err != nil {
		panic(err)
	}

	return &storageAlibabaOSS{
		client: client,
		bucket: bucket,
	}
}

func (s *storageAlibabaOSS) Get(bucketName string) (*oss.Bucket, error) {
	bucket, err := s.client.Bucket(bucketName)
	if err != nil {
		return nil, err
	}
	return bucket, nil
}

func (s *storageAlibabaOSS) Read(bucketName string, objectPath string) (io.ReadCloser, error) {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return nil, err
	}
	return bucket.GetObject(objectPath)
}

func (s *storageAlibabaOSS) Put(bucketName string, objectKey string, source io.Reader, options ...Option) error {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return err
	}
	var ossOptions []oss.Option
	for _, option := range options {
		if option == VisibilityPublic {
			ossOptions = append(ossOptions, oss.ObjectACL(oss.ACLPublicRead))
		}
	}
	return bucket.PutObject(objectKey, source, ossOptions...)
}

func (s *storageAlibabaOSS) Delete(bucketName string, objectKey string) error {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return err
	}
	return bucket.DeleteObject(objectKey)
}

func (s *storageAlibabaOSS) Copy(bucketName string, srcObjectPath string, dstObjectPath string) error {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return err
	}
	_, err = bucket.CopyObject(srcObjectPath, dstObjectPath)
	return err
}

func (s *storageAlibabaOSS) URL(bucketName string, objectPath string, isPublicObject bool) (string, error) {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return "", err
	}
	if isPublicObject {
		u := url.URL{
			Scheme: "https",
			Path:   path.Join(fmt.Sprintf("%s.%s", bucket.BucketName, bucket.GetConfig().Endpoint), objectPath),
		}
		return u.String(), nil
	}
	return bucket.SignURL(objectPath, oss.HTTPGet, signedURLExpiration)
}

func (s *storageAlibabaOSS) Size(bucketName string, objectPath string) (int64, error) {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return 0, err
	}
	r, err := bucket.GetObjectMeta(objectPath)
	if err != nil {
		return 0, err
	}

	sizeStr := r.Get("Content-Length")
	return strconv.ParseInt(sizeStr, 10, 64)
}

func (s *storageAlibabaOSS) LastModified(bucketName string, objectPath string) (time.Time, error) {
	bucket, err := s.Get(bucketName)
	if err != nil {
		return time.Time{}, err
	}
	r, err := bucket.GetObjectMeta(objectPath)
	if err != nil {
		return time.Time{}, err
	}

	LastModified, err := http.ParseTime(r.Get("Last-Modified"))
	if err != nil {
		return time.Time{}, err
	}

	return LastModified, nil
}
