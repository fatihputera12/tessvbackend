package framework

import (
	"bytes"
	"fmt"
	"io"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/sirupsen/logrus"
)

const (
	maxRetry   = 3           // maximum retry for uploading part
	s3PartSize = 5120 * 1024 // 5MB is minimum s3 part size upload
)

type storageS3 struct {
	awsSession *session.Session
	s3         *s3.S3
	bucketName string
}

// NewStorageAWSS3 create new storage backed by AWS S3
func NewStorageAWSS3(
	bucketName string,
	region string,
	accessKeyID string,
	secretAccessKey string,
	sessionToken string) Storage {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
		Credentials: credentials.NewStaticCredentials(
			accessKeyID,
			secretAccessKey,
			sessionToken,
		),
	})
	if err != nil {
		panic(err)
	}

	svc := s3.New(sess)
	return &storageS3{
		awsSession: sess,
		s3:         svc,
		bucketName: bucketName,
	}
}

func (s *storageS3) Read(bucketName string, objectPath string) (io.ReadCloser, error) {
	output, err := s.s3.GetObject(&s3.GetObjectInput{
		Bucket: &s.bucketName,
		Key:    &objectPath,
	})

	if err != nil {
		return nil, err
	}

	return output.Body, nil
}

func (s *storageS3) Put(bucketName string, objectPath string, source io.Reader, options ...Option) error {
	var acl *string = nil
	for _, option := range options {
		if option == VisibilityPublic {
			acl = aws.String(s3.BucketCannedACLPublicRead)
		}
	}

	expireAt := time.Now().Add(time.Hour * 6)
	createdResp, err := s.s3.CreateMultipartUpload(&s3.CreateMultipartUploadInput{
		ACL:     acl,
		Bucket:  &s.bucketName,
		Key:     &objectPath,
		Expires: &expireAt,
	})

	if err != nil {
		return err
	}

	var partNumber int64 = 1
	var completedParts []*s3.CompletedPart
	buffer := make([]byte, s3PartSize)
	for {

		bytesRead, err := source.Read(buffer)

		if err != nil && err != io.EOF {
			if err := abortMultipartUpload(bucketName, s.s3, createdResp); err != nil {
				logrus.Debugf("[S3] error aborting multipart upload, while reading data: %s\n", err.Error())
				return err
			}
			return err
		}

		if bytesRead <= 0 {
			break
		}

		completed, err := uploadMultipart(bucketName, s.s3, createdResp, buffer[:bytesRead], partNumber)
		if err != nil {
			if err := abortMultipartUpload(bucketName, s.s3, createdResp); err != nil {
				logrus.Debugf("[S3] error aborting multipart upload: %s\n", err.Error())
				return err
			}
			return err
		}

		partNumber++
		completedParts = append(completedParts, completed)
	}

	completionResp, err := s.s3.CompleteMultipartUpload(&s3.CompleteMultipartUploadInput{
		Bucket:   createdResp.Bucket,
		Key:      createdResp.Key,
		UploadId: createdResp.UploadId,
		MultipartUpload: &s3.CompletedMultipartUpload{
			Parts: completedParts,
		},
	})

	if err != nil {
		return err
	}

	logrus.Debugf("[S3] Upload success: %s\n", completionResp.String())
	return nil
}

func uploadMultipart(bucketName string, service *s3.S3, resp *s3.CreateMultipartUploadOutput, data []byte, partNumber int64) (*s3.CompletedPart, error) {
	uploadInput := &s3.UploadPartInput{
		Bucket:        resp.Bucket,
		Key:           resp.Key,
		UploadId:      resp.UploadId,
		Body:          bytes.NewReader(data),
		ContentLength: aws.Int64(int64(len(data))),
		PartNumber:    aws.Int64(partNumber),
	}

	var retry int
	for retry < maxRetry {
		logrus.Debugf("[S3] uploading (%d bytes) part %d - %s\n", len(data), partNumber, *resp.Key)
		uploadResp, err := service.UploadPart(uploadInput)

		if err != nil {
			retry++
			if retry >= maxRetry {
				return nil, err
			}
			time.Sleep(time.Second * 2)
			logrus.Debugf("[S3] retrying part %d - %s, err: %s\n", partNumber, *resp.Key, err.Error())
			continue
		}

		return &s3.CompletedPart{
			ETag:       uploadResp.ETag,
			PartNumber: &partNumber,
		}, nil
	}
	return nil, nil
}

func abortMultipartUpload(bucketName string, service *s3.S3, resp *s3.CreateMultipartUploadOutput) error {
	_, err := service.AbortMultipartUpload(&s3.AbortMultipartUploadInput{
		Bucket:   resp.Bucket,
		Key:      resp.Key,
		UploadId: resp.UploadId,
	})
	return err
}

func (s *storageS3) Delete(bucketName string, objectPath string) error {
	_, err := s.s3.DeleteObject(&s3.DeleteObjectInput{
		Bucket: &s.bucketName,
		Key:    &objectPath,
	})

	return err
}

func (s *storageS3) Copy(bucketName string, srcObjectPath string, dstObjectPath string) error {
	out, err := s.s3.CopyObject(&s3.CopyObjectInput{
		Bucket:     &s.bucketName,
		Key:        &dstObjectPath,
		CopySource: &srcObjectPath,
	})

	if err != nil {
		return err
	}

	logrus.Debug(out)
	return nil
}

func (s *storageS3) URL(bucketName string, objectPath string, isPublicObject bool) (string, error) {
	if isPublicObject {
		return fmt.Sprintf("https://%s.s3-%s.amazonaws.com/%s", s.bucketName, *s.awsSession.Config.Region, objectPath), nil
	}

	req, _ := s.s3.GetObjectRequest(&s3.GetObjectInput{
		Bucket: &s.bucketName,
		Key:    &objectPath,
	})

	return req.Presign(30 * time.Minute)
}

func (s *storageS3) Size(bucketName string, objectPath string) (int64, error) {
	output, err := s.s3.HeadObject(&s3.HeadObjectInput{
		Bucket: &s.bucketName,
		Key:    &objectPath,
	})
	if err != nil {
		return 0, err
	}

	logrus.Debug(output)
	return *output.ContentLength, nil
}

func (s *storageS3) LastModified(bucketName string, objectPath string) (time.Time, error) {
	output, err := s.s3.HeadObject(&s3.HeadObjectInput{
		Bucket: &s.bucketName,
		Key:    &objectPath,
	})
	if err != nil {
		return time.Time{}, err
	}

	return *output.LastModified, nil
}
