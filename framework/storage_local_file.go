package framework

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"time"

	"tsi-pagebuilder.com/api/app/shared/utils"
)

// LocalStorageSignedURLBuilder is used to serve file temporarily in private directory mode
type LocalStorageSignedURLBuilder func(absoluteFilePath string, objectPath string, expireIn time.Duration) (string, error)

type storageLocalFile struct {
	baseDir          string
	publicBaseDir    string
	publicBaseURL    string
	signedURLBuilder LocalStorageSignedURLBuilder
}

// NewStorageLocalFile create local file storage
func NewStorageLocalFile(baseDir string, publicBaseDir string, publicBaseURL string, signedURLBuilder LocalStorageSignedURLBuilder) Storage {
	if signedURLBuilder == nil {
		signedURLBuilder = func(absoluteFilePath string, objectPath string, expireIn time.Duration) (string, error) {
			return "", fmt.Errorf("[local-storage] unsupported signed url builder")
		}
	}

	return &storageLocalFile{
		baseDir:          baseDir,
		publicBaseDir:    publicBaseDir,
		publicBaseURL:    publicBaseURL,
		signedURLBuilder: signedURLBuilder,
	}
}

func (s *storageLocalFile) Read(bucketName string, objectPath string) (io.ReadCloser, error) {
	return os.Open(filepath.Join(s.baseDir, bucketName, objectPath))
}

func checkAndCreateParentDirectory(bucketName string, filePath string) error {
	fileDir := filepath.Dir(filePath)
	return utils.MkDirIfNotExists(fileDir)
}

func (s *storageLocalFile) Put(bucketName string, objectPath string, source io.Reader, options ...Option) error {
	filePath := filepath.Join(s.baseDir, bucketName, objectPath)
	if err := checkAndCreateParentDirectory(bucketName, filePath); err != nil {
		return err
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, source)
	if err != nil {
		return err
	}

	if len(options) > 0 {
		// If visibility public is on then create a link in public directory for this file to be served
		if options[0] == VisibilityPublic {
			publicPath := filepath.Join(s.publicBaseDir, bucketName, objectPath)
			if err := checkAndCreateParentDirectory(bucketName, publicPath); err != nil {
				return err
			}

			if runtime.GOOS == "linux" {
				absFilePath, err := filepath.Abs(filepath.ToSlash(filePath))
				if err != nil {
					return fmt.Errorf("[local-storage] err while creating abs path: %s", err)
				}

				if err := os.Symlink(absFilePath, publicPath); err != nil {
					return fmt.Errorf("[local-storage] err creating sym link: %s", err)
				}
			} else {
				// windows
				// In windows there's an issue in creating symbolic link
				// issue: "A required privilege is not held by the client"
				// therefore the easiest solution is create a copy/hard link
				if filePath != publicPath {
					if err := os.Link(filePath, publicPath); err != nil {
						return err
					}
					file.Close()

					if err := os.Remove(filePath); err != nil {
						return err
					}
				}
			}
		}
	}

	return err
}

func (s *storageLocalFile) Delete(bucketName string, objectPath string) error {
	return os.Remove(filepath.Join(s.baseDir, bucketName, objectPath))
}

func (s *storageLocalFile) Copy(bucketName string, srcObjectPath string, dstObjectPath string) error {
	sourceFilePath := filepath.Join(s.baseDir, bucketName, srcObjectPath)
	if err := checkAndCreateParentDirectory(bucketName, sourceFilePath); err != nil {
		return err
	}

	sourceStream, err := os.Open(sourceFilePath)
	if err != nil {
		return err
	}
	defer sourceStream.Close()

	destFilePath := filepath.Join(s.baseDir, bucketName, dstObjectPath)
	if err := checkAndCreateParentDirectory(bucketName, destFilePath); err != nil {
		return err
	}

	destStream, err := os.Open(destFilePath)
	if err != nil {
		return err
	}
	defer destStream.Close()

	_, err = io.Copy(destStream, sourceStream)
	return err
}

func (s *storageLocalFile) URL(bucketName string, objectPath string, isPublicObject bool) (string, error) {
	// filePath := filepath.Join(s.publicBaseDir, bucketName, objectPath)
	if !isPublicObject {
		return "", fmt.Errorf("the object is not public, please read and serve it manually")
	}

	u, err := url.Parse(s.publicBaseURL)
	if err != nil {
		return "", err
	}

	u.Path = path.Join(u.Path, bucketName, objectPath)

	return u.String(), nil
}

func (s *storageLocalFile) Size(bucketName string, objectPath string) (int64, error) {
	info, err := os.Stat(filepath.Join(s.baseDir, bucketName, objectPath))
	if err != nil {
		return 0, err
	}

	return info.Size(), nil
}

func (s *storageLocalFile) LastModified(bucketName string, objectPath string) (time.Time, error) {
	info, err := os.Stat(filepath.Join(s.baseDir, bucketName, objectPath))
	if err != nil {
		return time.Time{}, err
	}

	return info.ModTime(), nil
}
