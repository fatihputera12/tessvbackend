package framework

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
)

const nonce = "5564324565434564345624536254643525134631566536765689"

func GenerateHMAC(data string, hmacSecret string) (string, error) {
	if data == "" {
		return "", fmt.Errorf("error no data")
	}
	if hmacSecret == "" {
		return "", fmt.Errorf("error no hmac secret")
	}

	h := hmac.New(sha256.New, []byte(hmacSecret))
	h.Write([]byte(data + nonce))
	return base64.StdEncoding.EncodeToString(h.Sum(nil)), nil
}

type hmacAuthenticationData struct {
	ApiKey    string `json:"api_key"`
	Random    string `json:"random"`
	ExpiredAt int64  `json:"expired_at"`
}

const hmacAuthenticationDataLimiter = "."

func EncodeHmacAuthenticationHash(apiKey string, random string, expiredAt int64, hmacSecret string) (string, error) {
	// ============================= Generate data for hmac =============================
	dataForHmac := hmacAuthenticationData{
		ApiKey:    apiKey,
		Random:    random,
		ExpiredAt: expiredAt,
	}
	dataForHmacJson, err := json.Marshal(&dataForHmac)
	if err != nil {
		return "", fmt.Errorf("encode hmac data error: %s", err)
	}
	dataForHmacB64 := base64.StdEncoding.EncodeToString(dataForHmacJson)

	// ============================= Generate hmac =============================
	hmac, err := GenerateHMAC(dataForHmacB64, hmacSecret)
	if err != nil {
		return "", fmt.Errorf("hashing payload error: %s", err)
	}
	return strings.Join([]string{dataForHmacB64, hmac}, hmacAuthenticationDataLimiter), nil
}

func DecodeHmacAuthenticationHash(data string) (*hmacAuthenticationData, error) {
	splitData := strings.Split(data, hmacAuthenticationDataLimiter)
	if len(splitData) != 2 {
		return nil, fmt.Errorf("length of data incorrect")
	}

	// ============================= Decode data for hmac =============================
	dataForHmacB64 := splitData[0]
	dataForHmacJson, err := base64.StdEncoding.DecodeString(dataForHmacB64)
	if err != nil {
		return nil, fmt.Errorf("decode b64 error: %s", err)
	}
	var dataForHmac hmacAuthenticationData
	if err := json.Unmarshal(dataForHmacJson, &dataForHmac); err != nil {
		return nil, fmt.Errorf("decode json error: %s", err)
	}

	return &dataForHmac, nil
}
