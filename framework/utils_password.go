package framework

import (
	"crypto/sha256"
	"fmt"

	"github.com/gofrs/uuid"
	"golang.org/x/crypto/bcrypt"
)

// HashPassword hash privided password using bcrypt
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckPasswordHash check password with bcrypt hash
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func HashToken(token uuid.UUID) string {
	h := sha256.New()
	h.Write([]byte(token.String()))
	bs := h.Sum(nil)

	return fmt.Sprintf("%x", bs)
}
