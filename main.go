package main

import (
	"tsi-pagebuilder.com/api/cmd"
)

func main() {
	cmd.Execute()
}
