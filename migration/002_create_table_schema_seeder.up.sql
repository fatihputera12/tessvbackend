CREATE TABLE IF NOT EXISTS schema_seeders
(
    `id`         	bigint AUTO_INCREMENT PRIMARY KEY,
    `name` 		    varchar(255) unique not null,
    `created_at` 	TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
