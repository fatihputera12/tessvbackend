CREATE TABLE IF NOT EXISTS posts (
    `id`        BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `title`     VARCHAR(200) NOT NULL,
    `content`   TEXT NOT NULL,
    `category`  VARCHAR(100) NOT NULL,
    `status`    VARCHAR(100) NOT NULL comment 'publish,draft,trash',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    INDEX(deleted_at)
) ENGINE=INNODB; 